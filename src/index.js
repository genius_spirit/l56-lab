import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import BurgerBuilder from "./BurgerBuilder";

ReactDOM.render(<BurgerBuilder />, document.getElementById('root'));
registerServiceWorker();
