import React from 'react';
import './IngredientsControl.css';

const IngredientsControl = (props) => {

  return(
    <div className="Controls">
      <p>Current Price: {props.price}</p>
      {
        props.food.map((item, index) => {
          return (
            <div key={index} className='IngredientsControl'>
              <span className='IngredientsControl__title'>{item.type}</span>
              <button onClick={() => props.remove(index)}
                      className='IngredientsControl__btn'
                      disabled={item.amount === 0}
              >Less</button>
              <button onClick={() => props.click(index)} className='IngredientsControl__btn'>More</button>
            </div>
          )
        })
      }
    </div>
  )
};

export default IngredientsControl;