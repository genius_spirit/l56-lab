import React, { Component } from 'react';
import Ingredients from "./components/Ingridients/Ingridients";
import IngredientsControl from "./components/IngregientsControl/IngredientsControl";


class BurgerBuilder extends Component {

  state = {
    food: [
      {type: 'Salad', amount: 0, price: 5},
      {type: 'Cheese', amount: 0, price: 20},
      {type: 'Meat', amount: 0, price: 50},
      {type: 'Bacon', amount: 0, price: 30}
    ],
    totalPrice: 20,
    array: []
  };


  amountIncrease = (index) => {
    const food = [...this.state.food];
    const ingredients = {...food[index]};
    ingredients.amount++;
    food[index] = ingredients;

    let totalPrice = this.state.totalPrice;
    totalPrice += this.state.food[index].price;

    this.state.array.push(ingredients.type);
    this.setState({food, totalPrice});
  };

  removeIngredients = (index) => {
    const food = [...this.state.food];
    const ingredients = {...food[index]};
    ingredients.amount--;
    food[index] = ingredients;

    let totalPrice = this.state.totalPrice;
    totalPrice -= this.state.food[index].price;

    const array = [...this.state.array];
    const arr = [...array];
    const ingredientIndex =  arr.indexOf(ingredients.type);

    this.state.array.splice(ingredientIndex, 1);
    this.setState({food, totalPrice});
  };

  render() {
    return (
      <div>
        <Ingredients food={this.state.array}/>
        <IngredientsControl remove={this.removeIngredients} click={this.amountIncrease} food={this.state.food} price={this.state.totalPrice}/>
      </div>
    );
  }
}

export default BurgerBuilder;

